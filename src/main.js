import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Echo from 'laravel-echo'
import { createI18n } from 'vue-i18n'
import hu from './locales/hu.json'
import en from './locales/en.json'
import $ from 'jquery'
import vueDebounce from 'vue-debounce'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'malihu-custom-scrollbar-plugin'
import 'summernote'
import 'summernote/dist/summernote.min.css'
import './assets/css/style.css'
import '@fortawesome/fontawesome-free/js/all.js'
import '@fortawesome/fontawesome-free/css/all.css'
import './registerServiceWorker'
import Chart from 'chart.js';
import AudioRecorder from 'audio-recorder-polyfill'

if(!window.MediaRecorder)
    window.MediaRecorder = AudioRecorder


window.$ = $
window.jQuery = $

require ('@fancyapps/fancybox')
require('@fancyapps/fancybox/dist/jquery.fancybox.css');

if(localStorage.getItem('locale') == null) {
    localStorage.setItem('locale', 'hu')
}


const i18n = createI18n({
    locale: localStorage.getItem('locale'),
    fallbackLocale: 'hu',
    messages: {
     'hu': hu,
     'en': en
    }
})

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.VUE_APP_PUSHER_KEY,
    wsHost: process.env.VUE_APP_WS_HOST,
    wsPort: process.env.VUE_APP_WS_PORT,
    wssPort: process.env.VUE_APP_WS_PORT,
    wsPath: process.env.VUE_APP_WS_PATH,
    enabledTransports: ['ws', 'wss'],
    forceTLS: process.env.VUE_APP_USE_TLS === 'true',
    disableStats: true,
    authEndpoint: process.env.VUE_APP_API_URL+'/broadcasting/auth',
    auth: {
        headers: {
          Accept: 'application/json',
          Authorization: "Bearer " + localStorage.getItem('accessToken')
        }
    }
});

window.Echo.connector.pusher.connection.bind('connected', function () {
    axios.defaults.headers.common['X-Socket-Id'] = window.Echo.socketId();
});

axios.defaults.baseURL = process.env.VUE_APP_API_URL
axios.defaults.headers.common = {'Authorization': `Bearer ${localStorage.getItem('accessToken')}`, 'Accept': 'Application/json'}
store.state.token = localStorage.getItem('accessToken')

axios.interceptors.request.use(
    function (config) {
        config.params = config.params || {};
        config.params['locale'] = i18n.global.locale;
        return config;
    }, 
  function (error) {
    // Do something with request error
    return Promise.reject(error);
});

axios.interceptors.response.use(
    function (response) {
        if(response.data.message)
            store.state.toastNotifications.push({status: 'Success', message: response.data.message})
        return response;
    }, 
    function (error) {
        if(error.response.status !== 401 && error.response.status !== 403) {
            if(error.response.data.errors) {
                for(const field in error.response.data.errors) {
                    error.response.data.errors[field].forEach((msg) => store.state.toastNotifications.push({status: 'Error', message: msg}))
                }
            }     
            else if(error.response.data.message)
                store.state.toastNotifications.push({status: 'Error', message: error.response.data.message})
        }       
        else if (error.response.status == 401) {
            store.commit('setUser', null)
            console.log(router.currentRoute._value)
            if(router.currentRoute._value.name && (router.currentRoute._value.name !== 'Login' || router.currentRoute._value.name !== 'Register'))
                router.push({ name: 'Login' })
        }
        return Promise.reject(error);
    }
);

function routerLogic(to, from, next) {
    window.scrollTo(0, 0)
    console.log(`From: ${from.name} \n To: ${to.name}`)
    if(store.state.user) {
        if(store.state.user.email_verified_at) {
                if(to.name == 'Login' || to.name == 'Register' || to.name == 'VerifyEmail' || to.name == 'ForgotPassword' || to.name == 'ResetPassword' || (store.state.user.finished_registration && to.name == 'Register2'))
                    next({name: 'Home'})
                else if(
                    (
                        to.matched.some(record => record.meta.requiresAdminAccess) && 
                        store.state.user.role !== 'Admin'
                    ) ||
                    (
                        to.matched.some(record => record.meta.requiresMasterAccess) && 
                        store.state.user.role !== 'Master' &&
                        store.state.user.role !== 'Admin'
                    ) ||
                    (
                        to.matched.some(record => record.meta.requiresDLAUserAccess) && 
                        store.state.user.role !== 'DLA User' && 
                        store.state.user.role !== 'Master' &&
                        store.state.user.role !== 'Admin'
                    )
                )
                    next({name: 'Home'})
                else
                    next()
                
            
        }
        else {
            if(to.name == 'VerifyEmail')
                next()
            else
                next({name: 'VerifyEmail'})
        }
    }
    else {
        if(to.name == 'Register' || to.name == 'Login' || to.name == 'ForgotPassword' || to.name == 'ResetPassword')
            next()
        else
            next({name: 'Login'})
    }
   // if(to.name == 'Login' && store.state.user) next({name: 'Home'})
  //  else if(store.state.user && store.state.user.email_verified_at !== null && to.name == 'VerifyEmail') next({name: 'Home'})
   // else if(store.state.user && to.name == 'Register') next({name: 'Home'})
  //  else if(store.state.user && store.state.user.email_verified_at == null && to.name != 'VerifyEmail') next({name: 'VerifyEmail'})
   // else if(store.state.user && store.state.user.email_verified_at !== null && !store.state.user.finished_registration && to.name != 'Register2') next({name: 'Register2'})
   // else if(store.state.user && store.state.user.email_verified_at !== null && store.state.user.finished_registration && to.name == 'Register2') next({name: 'Home'})
   // else if (!(to.name === 'Login' || to.name === 'Register') && !store.state.user && !store.state.loading) next({ name: 'Login' })
  //  else if(to.matched.some(record => record.meta.requiresAdminAccess) && store.state.user.role !== 'Admin') next({name: from.name})
    //else if(
  //      to.matched.some(record => record.meta.requiresMasterAccess) && 
  //      store.state.user.role !== 'Master' &&
  //      store.state.user.role !== 'Admin'
   // ) 
  //      next({name: from.name})
   // else if(
   //     to.matched.some(record => record.meta.requiresDLAUserAccess) && 
   //     store.state.user.role !== 'DLA User' && 
   //     store.state.user.role !== 'Master' &&
   //     store.state.user.role !== 'Admin'
   // ) 
    //    next({name: from.name})
   // else next()
}

router.beforeEach((to, from, next) => {
    if(store.state.firstLoad) {
        store.state.firstLoad = false
        axios.get('/user').then(resp => {
            store.commit('setUser', resp.data)
            store.dispatch('setupWebsocketListeners')
        })
        .catch(err => console.log(err))
        .finally(() => {
            store.state.loading = false
            routerLogic(to, from, next)
        })
    }
    else {
        routerLogic(to, from, next)
    }
})

const app = createApp(App).use(store).use(router).use(VueAxios, axios).use(i18n).use(vueDebounce).mixin({
    data() {
        return {
            Chart: Chart
        }
    }
  }).mount('#app')

  store.state.i18n = app.$i18n


import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import CourseCatalog from '../views/CourseCatalog.vue'
import MyCourses from '../views/MyCourses.vue'
import Glossary from '../views/Glossary.vue'
import FastShare from '../views/FastShare.vue'
import AssignmentsToReturn from '../views/AssignmentsToReturn.vue'
import Reporting from '../views/Reporting.vue'
import MyTeam from '../views/MyTeam.vue'
import Course from '../views/Course.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Register2 from '../views/Register2.vue'
import VerifyEmail from '../views/VerifyEmail.vue'
import Chat from '../views/Chat.vue'
import Notifications from '../views/Notifications.vue'
import Settings from '../views/Settings.vue'
import ActivityHistory from '../views/ActivityHistory.vue'
import Overview from '../views/Overview.vue'
import Uploads from '../views/Uploads.vue'
import Meeting from '../views/Meeting.vue'
import GlossaryArticle from '../views/GlossaryArticle.vue'
import ForgotPassword from '../views/ForgotPassword.vue'
import ResetPassword from '../views/ResetPassword.vue'
import ViewCourse from '../views/ViewCourse.vue'
import CreateCourse from '../views/CreateCourse.vue'
import ManageCourses from '../views/ManageCourses.vue'
import CourseMaterials from '../views/CourseMaterials.vue'
import EditCourse from '../views/EditCourse.vue'
import TestAssembler from '../views/TestAssembler.vue'
import TestQuestionAssembler from '../views/TestQuestionAssembler.vue'
import EditTest from '../views/EditTest.vue'
import ManageTests from '../views/ManageTests.vue'
import ActiveTestAttempt from '../views/ActiveTestAttempt.vue'
import CorrectTestAttempt from '../views/CorrectTestAttempt.vue'
import ViewTestAttempt from '../views/ViewTestAttempt.vue'
import ManageUsers from '../views/ManageUsers.vue'
import CreateGlossaryArticle from '../views/CreateGlossaryArticle.vue'
import EditGlossaryArticle from '../views/EditGlossaryArticle.vue'
import CourseReport from '../views/CourseReport.vue'
import CourseReports from '../views/CourseReports.vue'
import UnapprovedGlossary from '../views/UnapprovedGlossary.vue'
import Cart from '../views/Cart.vue'
import AfterPayment from '../views/AfterPayment.vue'
import Feedback from '../views/Feedback.vue'
import FeedbackList from '../views/FeedbackList.vue'
import ResolvedFeedbackList from '../views/ResolvedFeedbackList.vue'
import CourseMetadataReports from '../views/CourseMetadataReports.vue'
import CourseFeedbackList from '../views/CourseFeedbackList.vue'
import EditCourseFeedback from '../views/EditCourseFeedback.vue'
import CreateUserCourseFeedback from '../views/CreateUserCourseFeedback'
import UserCourseFeedback from '../views/UserCourseFeedback'
import CourseFeedbackReports from '../views/CourseFeedbackReports'
import CourseQuizReports from '../views/CourseQuizReports'
import CourseQuizReport from '../views/CourseQuizReport'
import QuizReport from '../views/QuizReport'
import SearchReports from '../views/SearchReports'
import BarAssociationReports from '../views/BarAssociationReports'
import CourseBarAssociationReport from '../views/CourseBarAssociationReport'
import CheckoutUserData from '../views/CheckoutUserData.vue'
import CheckoutPaymentMethod from '../views/CheckoutPaymentMethod.vue'
import CheckoutSummary from '../views/CheckoutSummary.vue'
import AfterExam from '../views/AfterExam.vue'
import MyOrders from '../views/MyOrders.vue'
import Order from '../views/Order.vue'
import ViewPost from '../views/ViewPost.vue'
import PersonalAnalytics from '../views/PersonalAnalytics.vue'
import ManageNews from '../views/ManageNews.vue'
import CreateNews from '../views/CreateNews.vue'
import EditNews from '../views/EditNews.vue'
import ManageCoupons from '../views/ManageCoupons'
import CreateFarContract from '../views/CreateFarContract'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/feedback',
    name: 'Feedback',
    component: Feedback
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/checkout/data',
    name: 'CheckoutUserData',
    component: CheckoutUserData
  },
  {
    path: '/orders',
    name: 'MyOrders',
    component: MyOrders
  },
  {
    path: '/orders/:order',
    name: 'Order',
    component: Order
  },
  {
    path: '/checkout/payment-method',
    name: 'CheckoutPaymentMethod',
    component: CheckoutPaymentMethod
  },
  {
    path: '/checkout/summary',
    name: 'CheckoutSummary',
    component: CheckoutSummary
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/analytics/:user?',
    name: 'PersonalAnalytics',
    component: PersonalAnalytics
  },
  {
    path: '/forgot-password',
    name: 'ForgotPassword',
    props: true,
    component: ForgotPassword,
  },
  {
    path: '/reset-password',
    name: 'ResetPassword',
    component: ResetPassword,
    props: true
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/finish-registration',
    name: 'Register2',
    props: true,
    component: Register2
  },
  {
    path: '/test-attempt/active',
    name: 'ActiveTestAttempt',
    component: ActiveTestAttempt
  },
  {
    path: '/test-attempt/:testAttempt/corrected',
    name: 'ViewTestAttempt',
    component: ViewTestAttempt,
    props: true
  },
  {
    path: '/meeting/:id',
    name: 'Meeting',
    component: Meeting,
  },
  {
    path: '/verify-email',
    name: 'VerifyEmail',
    component: VerifyEmail
  },
  {
      path: '/course-catalog',
      name: 'CourseCatalog',
      component: CourseCatalog
  },
  {
      path: '/course/:id',
      name: 'Course',
      component: Course
  },
  {
    path: '/course/:id/view',
    name: 'ViewCourse',
    component: ViewCourse
  },
  {
    path: '/my-courses',
    name: 'MyCourses',
    component: MyCourses
  },
  {
    path: '/far-contract/:course/create',
    name: 'CreateFarContract',
    component: CreateFarContract
  },
  {
    path: '/notifications',
    name: 'Notifications',
    component: Notifications,
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
  },
  {
    path: '/activity-history/:id?',
    name: 'ActivityHistory',
    component: ActivityHistory,
  },
  {
    path: '/test/:test/attempt/:attempt/finished',
    name: 'AfterExam',
    props: true,
    component: AfterExam
  },
  {
    path: '/chat',
    name: 'Chat',
    props: true,
    component: Chat,
  },
  {
    path: '/cart',
    name: 'Cart',
    component: Cart,
  },
  {
    path: '/after-payment/:paymentRequestId?',
    name: 'AfterPayment',
    component: AfterPayment,
  },
  {
    path: '/uploads',
    name: 'Uploads',
    component: Uploads,
  },
  //DLA User routes
  {
    path: '/share',
    name: 'FastShare',
    component: FastShare,
    meta: { requiresDLAUserAccess: true }
  },
  {
    path: '/post',
    name: 'ViewPost',
    component: ViewPost,
    props: true,
    meta: { requiresDLAUserAccess: true }
  },
  {
    path: '/glossary',
    name: 'Glossary',
    component: Glossary,
    meta: { requiresDLAUserAccess: true }
  },
  {
    path: '/glossary/:id',
    name: 'GlossaryArticle',
    component: GlossaryArticle,
    meta: { requiresDLAUserAccess: true }
  },
  //Admin routes
  {
    path: '/coupons/manage',
    name: 'ManageCoupons',
    component: ManageCoupons,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/news/manage',
    name: 'ManageNews',
    component: ManageNews,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/news/create',
    name: 'CreateNews',
    component: CreateNews,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/news/:id/edit',
    name: 'EditNews',
    props: true,
    component: EditNews,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/feedback/list',
    name: 'FeedbackList',
    component: FeedbackList,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/feedback/resolved',
    name: 'ResolvedFeedbackList',
    component: ResolvedFeedbackList,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/course/:course/feedback/:feedback',
    name: 'UserCourseFeedback',
    component: UserCourseFeedback,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/course/:course/feedback/list',
    name: 'CourseFeedbackList',
    component: CourseFeedbackList,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/course/:course/feedback/edit',
    name: 'EditCourseFeedback',
    component: EditCourseFeedback,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/glossary/unapproved',
    name: 'UnapprovedGlossary',
    component: UnapprovedGlossary,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/users/manage',
    name: 'ManageUsers',
    component: ManageUsers,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/tests/manage',
    name: 'ManageTests',
    component: ManageTests,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/tests/create',
    name: 'TestAssembler',
    component: TestAssembler,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/tests/:test/edit',
    name: 'EditTest',
    component: EditTest,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/tests/:test/questions',
    name: 'TestQuestionAssembler',
    component: TestQuestionAssembler,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/assignments-to-return',
    name: 'AssignmentsToReturn',
    component: AssignmentsToReturn,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/reporting',
    name: 'Reporting',
    component: Reporting,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/reporting/search-results',
    name: 'SearchReports',
    component: SearchReports,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/reporting/bar-association',
    name: 'BarAssociationReports',
    component: BarAssociationReports,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/reporting/bar-association/:course',
    name: 'CourseBarAssociationReport',
    component: CourseBarAssociationReport,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/courses/create',
    name: 'CreateCourse',
    component: CreateCourse,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/courses/manage',
    name: 'ManageCourses',
    component: ManageCourses,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/courses/:course/edit',
    name: 'EditCourse',
    component: EditCourse,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/courses/metadata',
    name: 'CourseMetadataReports',
    component: CourseMetadataReports,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/courses/quiz/reports',
    name: 'CourseQuizReports',
    component: CourseQuizReports,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/courses/:course/quiz/reports',
    name: 'CourseQuizReport',
    component: CourseQuizReport,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/courses/:course/quiz/:test/reports',
    name: 'QuizReport',
    component: QuizReport,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/courses/feedback/reports',
    name: 'CourseFeedbackReports',
    component: CourseFeedbackReports,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/courses/reports',
    name: 'CourseReports',
    component: CourseReports,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/courses/:course/report',
    name: 'CourseReport',
    component: CourseReport,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/courses/:course/materials',
    name: 'CourseMaterials',
    component: CourseMaterials,
    meta: { requiresAdminAccess: true }
  },
  {
    path: '/glossary/:id/edit',
    name: 'EditGlossaryArticle',
    component: EditGlossaryArticle,
    meta: { requiresAdminAccess: true }
  },
  //Master routes
  {
    path: '/test-attempts/:testAttempt/correct',
    name: 'CorrectTestAttempt',
    component: CorrectTestAttempt,
    meta: { requiresMasterAccess: true }
  },
  {
    path: '/my-team',
    name: 'MyTeam',
    component: MyTeam,
    meta: { requiresMasterAccess: true }
  },
  {
    path: '/overview/:id',
    name: 'Overview',
    component: Overview,
    meta: { requiresMasterAccess: true }
  },
  {
    path: '/glossary/create',
    name: 'CreateGlossaryArticle',
    component: CreateGlossaryArticle,
    meta: { requiresMasterAccess: true }
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router

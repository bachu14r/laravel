module.exports = {
  lintOnSave: false,
  pages: {
    index: {
      entry: 'src/main.js',

      template: 'public/index.html',

      filename: 'index.html',

      title: 'Home Page',

      chunks: ['chunk-vendors', 'chunk-common', 'index']
    },
    meeting: {
        entry: 'src/main.js',

        template: 'public/zoom.html',

        filename: 'zoom.html',

        title: 'Meeting',

        chunks: ['chunk-vendors', 'chunk-common', 'index']
    }
  },
  pwa: {
    name: 'Develawp',
    themeColor: '#176fb7',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    iconPaths: {
        favicon32: 'img/icons/favicon-32x32.png',
        favicon16: 'img/icons/favicon-16x16.png',
        appleTouchIcon: 'img/icons/apple-touch-icon-152x152.png',
        maskIcon: 'img/icons/maskable-icon.png',
        msTileImage: 'img/icons/ms-icon-144x144.png'
    },
    manifestOptions: {
        icons: [
            {
                "src": "img/icons/maskable-icon.png",
                "sizes": "189x189",
                "type": "image/png",
                "purpose": "maskable"
            },
            {
              "src": "img/icons/icon-72x72.png",
              "sizes": "72x72",
              "type": "image/png"
            },
            {
              "src": "img/icons/icon-96x96.png",
              "sizes": "96x96",
              "type": "image/png"
            },
            {
              "src": "img/icons/icon-128x128.png",
              "sizes": "128x128",
              "type": "image/png"
            },
            {
              "src": "img/icons/icon-144x144.png",
              "sizes": "144x144",
              "type": "image/png"
            },
            {
              "src": "img/icons/icon-152x152.png",
              "sizes": "152x152",
              "type": "image/png"
            },
            {
              "src": "img/icons/icon-192x192.png",
              "sizes": "192x192",
              "type": "image/png"
            },
            {
              "src": "img/icons/icon-384x384.png",
              "sizes": "384x384",
              "type": "image/png"
            },
            {
              "src": "img/icons/icon-512x512.png",
              "sizes": "512x512",
              "type": "image/png"
            }
          ]
    },

    // configure the workbox plugin
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'service-worker.js',
    },
  }
}

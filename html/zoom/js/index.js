ZoomMtg.setZoomJSLib('https://dmogdx0jrul3u.cloudfront.net/1.8.1/lib', '/av');
ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();

const meetConfig = {
	apiKey: 'rw6qXVG3SYC7H-DEKkGiDA',
	meetingNumber: '73142770283',
	leaveUrl: 'https://yoursite.com/meetingEnd',
	userName: 'Firstname Lastname',
	userEmail: 'firstname.lastname@yoursite.com',
	passWord: 'QWp0aFFmWC9RblEzWDZVU2tTNlA3dz09', // if required
	role: 0 // 1 for host; 0 for attendee
};

function getSignature(meetConfig) {
	fetch(`https://invoice.weborigo.eu/zoom/api/signature.php?meetingNumber=${meetConfig.meetingNumber}&role=${meetConfig.role}`, {
			method: 'POST'
		})
		.then(result => result.text())
		.then(response => {
            console.log(JSON.parse(response).signature)
			ZoomMtg.init({
				leaveUrl: meetConfig.leaveUrl,
				isSupportAV: true,
				success: function() {
					ZoomMtg.join({
						signature: JSON.parse(response).signature,
						apiKey: meetConfig.apiKey,
						meetingNumber: meetConfig.meetingNumber,
						userName: meetConfig.userName,
						passWord: meetConfig.passWord,
						error(res) { 
							console.log(res) 
						}
					})		
				}
			})
	})
}

getSignature(meetConfig);

<?
    header("Content-type: application/json");
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, *");

    $api_key = "rw6qXVG3SYC7H-DEKkGiDA";
    $api_secret = "3b1lMU6VxCv7zfpMvdKjqsVMS493ndg62nzG";

    $meeting_number = isset($_REQUEST['meetingNumber']) ? $_REQUEST['meetingNumber'] : null;
    $role = isset($_REQUEST['role']) ? $_REQUEST['role'] : null;

    function generate_signature($api_key, $api_secret, $meeting_number, $role) {

        $time = time() * 1000 - 30000; //time in milliseconds (or close enough)

        $data = base64_encode($api_key . $meeting_number . $time . $role);

        $hash = hash_hmac('sha256', $data, $api_secret, true);

        $_sig = $api_key . "." . $meeting_number . "." . $time . "." . $role . "." . base64_encode($hash);

        return rtrim(strtr(base64_encode($_sig), '+/', '-_'), '=');
    }

    if(($meeting_number !== null) && ($role !== null)) {
        $statement['signature'] = generate_signature($api_key, $api_secret, $meeting_number, $role);
    }
    else {
        $statement['signature'] = "INVALID_PARAMS";   
    }

    echo json_encode($statement, JSON_PRETTY_PRINT);
?>